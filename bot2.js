var fs=require('fs');
var jsdom=require('jsdom');
var jq='http://code.jquery.com/jquery-1.5.min.js';

fs.readFile("./views/menu.ejs", function(err, data) {
    jsdom.env({
    		html:data,
				scripts: [jq]
			}, function (err,window) {
					var $ = window.jQuery;
          
          var cars={};
          
          $('.make').each(function(){
            var make=$(this).text();
            cars[make]=[];
            
            $(this).next().children().each(function(){
              var size=$(this).children().first();
              var sizeText=size.text();
              size.next().children().each(function(){
                var model=$(this).text();
                cars[make].push({model:model,size:sizeText});
              });
            });
          });
          
          window.close();
          //console.log(cars);
          
          fs.writeFile("carJson2.json", JSON.stringify(cars, null, 2), function(err) {
            if (err) {
              console.log(err);
            }
            else {
              console.log("The file was saved!");
            }
          }); 
			});
});