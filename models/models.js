var mongoose = require('mongoose')
    ,Schema = mongoose.Schema
    ,ObjectId = Schema.ObjectId;

var carSchema = new mongoose.Schema({
    make : String,
    model: String,
    size: String,
    price: Number,
    year: Number,
    details: String,
    created_at : {type: Date, default: Date.now},
    deleteTimeout : Number
});

var userSchema = new Schema({
    login: String,
    password: String,
    name: String,
    cars: [ObjectId]
});

exports.Car = mongoose.model('Car', carSchema);
exports.User = mongoose.model('User', userSchema);
