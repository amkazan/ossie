// The User model

var mongoose = require('mongoose')
    ,Schema = mongoose.Schema
    ,ObjectId = Schema.ObjectId;

var userSchema = new Schema({
    name: String,
    recipes: [ObjectId]
});

module.exports = mongoose.model('User', userSchema);