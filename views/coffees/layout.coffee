doctype 5
html ->
  head ->
    script src: 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'
    script src: 'http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/jquery.dataTables.min.js'
    #link rel: 'stylesheet', href: 'http://datatables.net/release-datatables/media/css/demo_table.css'
    link rel : 'stylesheet', href : 'http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables.css'
    
    coffeescript ->
      $(document).ready ->
        $("#example").dataTable()
        $("#example2").dataTable()
    #if !@session.user
    div ->
      a href:'/myCars', 'My cars'
    div -> 
      a href: '/addCarMenu', 'Add car'
    if @session.user
      div '#logindiv', ->
        div ->
          user= 'logged in as ' + @session.user.login
        div ->
          a href:'/sessions/destroy', 'logout'
        div ''
    if @warn
      @warn
    @body
###    meta charset: 'utf-8'
    title "#{@title or 'Untitled'} | My awesome website"
    meta(name: 'description', content: @desc) if @desc?
    link rel: 'stylesheet', href: '/stylesheets/app.css'
    style '''
      body {font-family: sans-serif}
      header, nav, section, footer {display: block}
    '''
    script src: '/javascripts/jquery.js'
    coffeescript ->
      alert '1'
      #$ ->
      #  alert 'Alerts are so annoying...'
      #alert '1'
  body ->
    header ->
      h1 @title or 'Untitled'
      nav ->
        ul ->
          (li -> a href: '/', -> 'Home') unless @path is '/'
          li -> a href: '/chunky', -> 'Bacon!'
          switch @user.role
            when 'owner', 'admin'
              li -> a href: '/admin', -> 'Secret Stuff'
            when 'vip'
              li -> a href: '/vip', -> 'Exclusive Stuff'
            else
              li -> a href: '/commoners', -> 'Just Stuff'
    section ->
      h2 "Let's count to #{@max}:"
      p i for i in [1..@max]
    footer ->
      p shoutify('bye')###