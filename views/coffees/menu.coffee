html dir: 'ltr', ->
  head ->
    comment 'Start css3menu.com HEAD section'
    link rel: 'stylesheet', href: 'stylesheets/style.css', type: 'text/css'
    script src: 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'
    style '._css3m{display:none}'
    comment 'End css3menu.com HEAD section'
  coffeescript ->
    $ ->
      $('a').click ->
        alert .tagName
        if $(this).next().get(0)==undefined && $(this).prev().get(0) == undefined
          window.location.replace '/myCars?model=' + $(this).text()
  body style: 'background-color:#EBEBEB', ->
    comment 'Start css3menu.com BODY section'
    ul '#css3menu1.topmenu', ->
      li '.topfirst', ->
        a href: '#', style: 'height:18px;line-height:18px;', ->
          span 'Top 12 Makes'
        ul ->
          li ->
            a href: '#', ->
              span 'Ford'
            ul ->
              li ->
                a href: '#', ->
                  span 'Small'
                ul ->
                  li ->
                    a href: '#', 'Ka'
              li ->
                a href: '#', ->
                  span 'Standard'
                ul ->
                  li ->
                    a href: '#', 'Focus'
                  li ->
                    a href: '#', 'Fiesta'
                  li ->
                    a href: '#', 'Fusion'
              li ->
                a href: '#', ->
                  span 'Large'
                ul ->
                  li ->
                    a href: '#', 'Galaxy'
                  li ->
                    a href: '#', ->
                      span 'Mondeo'
                    ul ->
                      li ->
                        a href: 'flrg1E.html', 'Mondeo Estate'
          li ->
            a href: '#', 'Vauxhall'
          li ->
            a href: '#', ->
              span 'VW'
            ul ->
              li ->
                a href: '#', 'Item 2 7 0'
          li ->
            a href: '#', 'BMW'
          li ->
            a href: '#', 'Peugeot'
          li ->
            a href: '#', 'Renault'
          li ->
            a href: '#', 'Mercedes'
          li ->
            a href: '#', 'Audi'
          li ->
            a href: '#', 'Citroen'
          li ->
            a href: '#', 'Volvo'
      li ->
        a href: '#', style: 'height:18px;line-height:18px;', ->
          span 'Top Asian'
        ul ->
          li ->
            a href: '#', ->
              span 'Toyota'
            ul ->
              li ->
                a href: '#', 'Small'
              li ->
                a href: '#', 'Standard'
              li ->
                a href: '#', ->
                  span 'Large'
                ul ->
                  li ->
                    a href: '#', ->
                      span 'Avensis'
                    ul ->
                      li ->
                        a href: 'tylrg1E.html', 'Avensis Estate'
          li ->
            a href: '#', 'Nissan'
          li ->
            a href: '#', 'Honda'
          li ->
            a href: '#', 'Mazda'
          li ->
            a href: '#', 'Hyundai'
          li ->
            a href: '#', 'Kia'
          li ->
            a href: '#', 'Mitsubusihi'
          li ->
            a href: '#', 'Suzuki'
          li ->
            a href: '#', 'Subaru'
          li ->
            a href: '#', 'Daewoo'
          li ->
            a href: '#', 'Daihatsu'
          li ->
            a href: '#', 'Lexus'
      li ->
        a href: '#', style: 'height:18px;line-height:18px;', ->
          span 'Popular'
        ul ->
          li ->
            a href: '#', 'Fiat'
          li ->
            a href: '#', 'Seat'
          li ->
            a href: '#', 'Skoda'
          li ->
            a href: '#', 'Saab'
          li ->
            a href: '#', 'Alpha Romeo'
          li ->
            a href: '#', 'Chevrolet'
          li ->
            a href: '#', 'Chrysler'
      li ->
        a href: '#', style: 'height:18px;line-height:18px;', ->
          span 'Old English'
        ul ->
          li ->
            a href: '#', 'LandRover'
          li ->
            a href: '#', 'Rover'
          li ->
            a href: '#', 'Jaguar'
          li ->
            a href: '#', 'MG'
          li ->
            a href: '#', 'Bentley'
          li ->
            a href: '#', 'Rolls Royce'
      li '.toplast', ->
        a href: '#', style: 'height:18px;line-height:18px;', ->
          span 'Other'
        ul ->
          li ->
            a href: '#', 'Porsche'
          li ->
            a href: '#', 'Smart'
          li ->
            a href: '#', 'Jeep'
          li ->
            a href: '#', 'Dodge'
          li ->
            a href: '#', 'Isuzu'
          li ->
            a href: '#', 'Proton'
    p '._css3m', ->
      a href: 'http://css3menu.com/', 'CSS For Menus Css3Menu.com'
    comment 'End css3menu.com BODY section'
