coffeescript ->
  $ ->
    $('body').css 'padding-left', '25%'
    $('body').css 'padding-right', '25%'

table '#example2.display',cellpadding :"0", cellspacing:"0", border:"0", ->
  thead ->
    tr ->
      th -> 'Make'
      th -> 'Model'
      th -> 'Price'
      th -> 'Year'
      th -> 'Details'
  partial 'car', cars
  tfoot ->
    tr ->
      th -> 'Make'
      th -> 'Model'
      th -> 'Price'
      th -> 'Year'
      th -> 'Details'