tr id:"#{@car._id}",->
  td -> @car.make
  td -> @car.model
  td ".center", -> 
    text '£'
    "#{@car.price}"
  td ".center", -> "#{@car.year}"
  td ".center", -> 
    a href : @car.details, -> text 'see'