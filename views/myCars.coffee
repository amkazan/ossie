link rel: 'stylesheet', href: 'stylesheets/style.css', type: 'text/css'

coffeescript ->
  $ ->
    list= [
      Ford:'Fiesta'
    ,
      Ford:'Focus'
    ,
      Ford:'Fusion'
    ,
      Ford:'Galaxy'
    ,
      Ford:'Ka'
    ,
      Ford:'Mondeo'
    ,
      BMW:'X5'
    ,
      BMW:'X6'
    ,
    ]
        
    
    if $('#carModel').attr('name')
      for i of list
        for k of list[i]
          if list[i][k] is $('#carModel').attr 'name'
            $('#makeSelect').val k
            choose = $ "#chooseModel"
            choose.html ''
            choose.append $("<option>Choose model >></option>")
            m = 0
            while m < list.length
              choose.append $("<option>" + list[m][k] + "</option>")  if list[m][k]
              m++
                        
            $('#chooseModel').val list[i][k]

      
    $('#makeSelect').change ->
      val = $(this).val()
      choose = $ "#chooseModel"
      choose.html ''
      choose.append $("<option>Choose model >></option>")
      
      i = 0
      while i < list.length
        choose.append $("<option>" + list[i][val] + "</option>")  if list[i][val]
        i++
        
    $('tr').click ->
      for nn of $(this).parent().children()
        el = $(this).parent().children().get nn
        if !$(el).text()
          break
        #$(el).removeClass 'trSel'
        $(el).children().attr 'style', ''
      $(this).addClass 'trSel'
      $(this).children().attr 'style', 'background-color:gray'
      
      $('#makeSelect').val $(this).children().eq(0).text()
      $('#makeSelect').trigger 'change'
      $('#chooseModel').val $(this).children().eq(1).text()
      $('#yearSelect').val $(this).children().eq(3).text()
      $('#priceSelect').val $(this).children().eq(2).text()
      $('#detailsSelect').val $(this).children().eq(4).children().eq(0).attr 'href'
      
      $('#selectedIdd').val $(this).attr 'id'
      
    $('#updateId').click (e) ->
      mes=$('#selectedIdd').val()
      if mes == "null"
        alert 'Please, choose a car'
        e.preventDefault();
    
    $('#deleteId').click (e) ->
      mes=$('#selectedIdd').val()
      if mes == "null"
        alert 'Please, choose a car'
        e.preventDefault();

input '#carModel',type:'hidden', name:@carModel

form '#form1',name:"form1", method:"post", action:"addCar", ->
  select '#makeSelect',name:'make', ->
    option 'Choose make >>'
    option 'Audi'
    option 'BMW'
    option 'Citroen'
    option 'Ford'
    option 'Mazda'
    option 'Nissan'
    option 'Peougeot'
    option 'Toyota'
  select '#chooseModel',name:'model', ->
    option 'Choose model >>'
  select '#yearSelect', name:'year', ->
    option 'Choose year >>'
    option '2012'
    option '2011'
    option '2010'
    option '2009'
    option '2008'
    option '2007'
    option '2006'
    option '2005'
    option '2004'
    option '2003'
    option '2002'
    option '2001'
    option '2000'
    option '1999'
    option '1998'
  span '.currencyinput', ->
    text '£'
  input '#priceSelect',type:'text', name:"price"
  input '#detailsSelect', type:'text', name:'details'
  #input type : "submit", value:"save"
  input type : 'submit', id: 'createId', value:'create',name :'create'
  input type : 'submit', id: 'updateId' ,value:'update',name:'update'
  input type : 'submit', id: 'deleteId' ,value:'delete',name:'delete'
  input '#selectedIdd',type:'hidden', name:'selectedIdd', value :'null'
  
table '#example.display',cellpadding :"0", cellspacing:"0", border:"0", ->
  thead ->
    tr ->
      th -> 'Make'
      th -> 'Model'
      th -> 'Price'
      th -> 'Year'
      th -> 'Details'
  if @cars
    partial 'car', cars
  tfoot ->
    tr ->
      th -> 'Make'
      th -> 'Model'
      th -> 'Price'
      th -> 'Year'
      th -> 'Details'
    