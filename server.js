var express = require('express'),
    mongoose = require('mongoose'),
    routes = require('./routes'),
    sys=require('sys');

var app = module.exports = express.createServer();

mongoose.connect('mongodb://localhost/db');
//mongoose.connect('mongodb://amkazan:12345678@ds031777.mongolab.com:31777/recipes');

var MemStore = express.session.MemoryStore;
//var MemoryStore = require('connect/middleware/session/memory');
//app.use(express.session({        secret: 'secret_key',        store : MemStore({            reapInterval : 60000*10        })    }))

// Configuration
app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  //app.register ('.ejs', require('ejs').adapters.express);
  app.register ('.ejs', require('ejs'));
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.use(express.session({ secret: "keyboard cat", store: MemStore({reapInterval : 60000*10}) }));
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

app.dynamicHelpers(
  {
    session: function(req, res) {
      return req.session;
    },
    
    flash: function(req, res) {
      return req.flash();
    },
    
    warn: function (req, res) {
        return req.flash('warn');
    },
    
    gCars: function (){
      return routes.GlobalCars();
    }
  }
);

// Routes
app.get('/sessions/new',routes.sessionsNew)
app.get('/myCars',routes.requiresLogin,routes.getMyCars);
app.get('/addCarMenu',routes.requiresLogin,routes.addCarMenu);
app.post('/sessions',routes.sessions);
app.post('/addCar',routes.requiresLogin,routes.addCar);
app.get('/sessions/destroy', routes.sessionDestroy);
app.get('/cars.json',function(req,res){
  res.send(routes.GlobalCars());
});

app.get('/',routes.index);
//app.get('/index2', routes.index2);
//app.get('/add',routes.add);


var port = process.env.PORT || 8080;
app.listen(8080, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
  console.log("http://127.0.0.1:8080");
});