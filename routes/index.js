var jq='http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js';

var models = require('../models/models.js');
var sys=require('sys');
var fs=require('fs');
var twitter = require('ntwitter');

var globalCars;
exports.GlobalCars=function(){return globalCars;}

var twit = new twitter({
  consumer_key: 'dcZGRAm3dDWNnjDP82tvg',
  consumer_secret: 'HkSBElR80qv21xV2K6izXjcFq7ysxRpowR1wL3NQZ4Y',
  access_token_key: '559367682-rFQBZrnjdo35FkjQco0y65oKFWad3AMPrRNrQzDe',
  access_token_secret: 'wjcsuJhLIjBM22JCuNwKMM9T6Yo6kQPDFw88bjHJM'
});

fs.readFile("./public/json/carJson2.json", function(err, data) { 
    globalCars=JSON.parse(data);
});

// Autodelete feature
setInterval(function(){
  models.Car.find({},function(err,cars) {
    for(var c in cars){
      var created_at = cars[c].created_at.getTime();
      var now = Date.now();
      var deleteTimeout = cars[c].deleteTimeout*24*60*60*1000;
      
      if(now > created_at+deleteTimeout)
      {
        cars[c].remove();
        console.log('car removed. autodelete time passed');
      }
    }
  });
}, 1000000);

function getSize(make,model){
  for (var i in globalCars[make]){
    if(globalCars[make][i].model==model){
      var size=globalCars[make][i].size;
      return size;
    }
  }
  
  throw new Error('error');
}

var authenticate = function(log, password, callback) {
  models.User.findOne({login:log},function(err,user) {
    if (!user) {callback(null);return;}
    if (user.password == password) {
      callback(user);
      return;
    }
    callback(null);
  }); 
};


exports.sessions=function(req, res) {
  authenticate(req.body.login, req.body.password, function(user) {
    if (user) {
      req.session.user = user;
      res.redirect(req.body.redir || '/');
    } else {
      req.flash('warn', 'Login failed');
      res.render('sessions/new', {layout:'layout2',locals: {redir: req.body.redir}});
    }
  });
};

exports.sessionsNew = function(req, res) {
  res.render('sessions/new', {layout:'layout2',locals: {
    redir: req.query.redir
  }});
};

exports.sessionDestroy = function(req, res) {
  delete req.session.user;
  res.redirect('/');
};

exports.requiresLogin = function (req, res, next) {
  if (req.session.user) {
    next();
  } else {
    if(req.url=="/addCar") req.url="/";
    res.redirect('/sessions/new?redir=' + req.url);
  }
};

exports.index = function(req, res) {
  models.Car.find({},function(err,cars) {
    res.render('index',{locals: {title: 'Carsss',cars: cars}});
  });
};

exports.index2 = function(req, res) {
  models.Car.find({},function(err,cars) {
    if (cars) {
      res.render('index2',{locals: {title: 'Carsss',cars: cars}});
    }
  });
};

exports.add = function (req,res) {
  var u = new models.User({login:req.session.user.login,password:"pa"});
  u.save();
};

exports.getMyCars = function(req,res){
  var model=req.query['model'];
  var make=req.query['make'];

  models.User.findOne({login:req.session.user.login},function(err,user) {
    models.Car.find({_id:{$in: user.cars}},function(err,cars){
      //console.log(user.cars);
      //console.log(cars);
      res.render('myCars',{layout:'layout2',locals: {carModel:model,carMake:make,globalCars:globalCars,title: 'Carsss',cars: cars}});
    });
  });
};

exports.addCar = function(req,res){  
  var c;
  if(req.body.details.indexOf('http')==-1 && req.body.details.length!=0){
    req.body.details='http://'+req.body.details;
  }
  
  if(req.body.create){
    var size=getSize(req.body.make,req.body.model);
  
    var c = new models.Car({size:size,make:req.body.make,model:req.body.model,price:req.body.price,year:req.body.year,details:req.body.details,deleteTimeout:50});  
    c.save(function(err) {
    models.User.findOne({login:req.session.user.login},function(err,user) {
      if(!user.cars) user.cars=[];
      user.cars.push(c._id);
      user.markModified();
      user.save(function(err) {
        //console.log(user.cars);
        if(!err){
          twit.verifyCredentials(function(err, data) {
            if (err) {
              console.log("Error verifying credentials: " + err);
            }
          }).updateStatus('New car: '+c.make+' '+ c.model+' '+c.year+"– £"+c.price + ' ' + 'www.cars-inretford.co.uk', function(err, data) {
            if (err) console.log('Tweeting failed: ' + err);
            else console.log('Success!')
          });
        }
        res.redirect('/myCars');
      });
    });
  });  
  }
  
  
  
  if(req.body.update){
    var size=getSize(req.body.make,req.body.model);
    models.Car.findById(req.body.selectedIdd,function(err,c){
      c.make=req.body.make;
      c.model=req.body.model;
      
      if(c.price!=req.body.price){
        c.deleteTimeout+=15;
        if(c.deleteTimeout>80) c.deleteTimeout=80;
      }
      
      c.price=req.body.price;
      c.year=req.body.year;
      c.size=size;
      c.details=req.body.details;
      c.save(function(err){
        res.redirect('/myCars');
      });
    });
  }
  
  if (req.body.delete) {
    models.User.findOne({login: req.session.user.login}, function(err, user) {
      models.Car.findById(req.body.selectedIdd, function(err, c) {
        c.remove(function() {
          user.cars.splice(user.cars.indexOf(req.body.selectedIdd),1);
          user.save(function(){
            res.redirect('/myCars');
          });
        });
      });
    });
  }
  
  
};

exports.addCarMenu = function(req,res){
  res.render('menu',{ layout:'layout2',layout: false });
};